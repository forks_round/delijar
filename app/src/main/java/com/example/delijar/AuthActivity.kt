package com.example.delijar

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_auth.*

enum class AuthType {
    SignUp,
    SignIn
}

class AuthActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        auth = Firebase.auth
    }

    override fun onStart() {
        super.onStart()

        val currentUser = auth.currentUser

        //if (currentUser != null)
    }

    fun RegisterUser(view: View) {
        val email = eTxt_Email.text.toString()
        val password = eTxt_Password.text.toString()
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    actionSuccessful(AuthType.SignUp, user)
                } else {
                    showErrorAuth(task)
                }
            }
    }

    fun EnterUser(view: View) {
        val email = eTxt_Email.text.toString()
        val password = eTxt_Password.text.toString()
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    actionSuccessful(AuthType.SignIn, user)
                } else {
                    showErrorAuth(task)
                }
            }
    }

    private fun showErrorAuth(task: Task<AuthResult>) {
        Log.w(TAG, "Error de autenticacion", task.exception)
        Toast.makeText(baseContext, "Authentication failed.",
            Toast.LENGTH_SHORT).show()
    }

    private fun actionSuccessful(authType:AuthType, user:FirebaseUser?) {
        if (authType.name.toString() == AuthType.SignUp.toString()) {
            Log.d(TAG, "createUserWithEmail:success ${user?.email}")
        }
        else if (authType.name.toString() == AuthType.SignIn.toString()) {
            Log.d(TAG, "signInWithEmail:success ${user?.email}")
            val intentAuth = Intent(AuthActivity@this, HomeActivity::class.java).apply {
                putExtra("email", user?.email)
            }
            startActivity(intentAuth)
        }
    }

}